# Custom buckets

![bitbucket_default_repos](http://f.cl.ly/items/18040O2M141V3G2V1b2H/bitbucket_default_repos.png)

Here are all the customized language buckets for your repository avatars.

## Contribute to the buckets

* Fork the repository
* Make a pretty new bucket image
* Make a pull request with a images of your work

### Notes for working with the illustrator file

Use an envelope distort to wrap your image around the bucket.

* Envelope Distort: Warp options
* Style: Arch
* ✔ Horizontal
* Bend: -18%
* Horizontal: 0
* Vertical: -8

When importing to photoshop, transform by 275% to reach 512px

### Exports

We need a different resolution bucket at the following sizes:

* 16
* 24
* 32
* 48
* 64
* 128
* 256
* 512
